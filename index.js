/**
 * An easy console app that shows current temperature and relative humidity
 * from the nearest Xiaomi Mi Bluetooth Temperature and Humidity Sensors.
 *
 * @author Ivan Semenov <developer.ivan.semenov@gmail.com>
 */

const noble = require('noble');

noble.startScanning();
noble.on('discover', function(peripheral) {
  if (peripheral.advertisement.localName === 'MJ_HT_V1') {
    console.log('The nearest peripheral: ', peripheral);
    console.log('Connecting...\n');
    peripheral.connect(function(error) {
      console.log('ERROR: ', error);
      console.log('Connected peripheral: ', peripheral);
      console.clear();

      setInterval((peripheral) => {
        const serviceData = peripheral.advertisement.serviceData;
        const serviceDataIndex = serviceData.findIndex(service => service.uuid === 'fe95');
        const receivedFromSensor = serviceData[serviceDataIndex].data;

        if (receivedFromSensor.length === 18) {
          const rawTemperature = receivedFromSensor.readUIntLE(14, 2);
          const rawHumidity = receivedFromSensor.readUInt16LE(16);

          const temperature = parseInt(rawTemperature) / 10;
          const humidity = parseInt(rawHumidity) / 10;

          process.stdout.clearLine();
          process.stdout.cursorTo(0);
          process.stdout.write(
            `Temperature: ${temperature} C; Humidity: ${humidity}%; Device: ${peripheral.address}`
          );
        }
      }, 2000, peripheral)
    })
  }
});
